const path = require('path');
const slueStream = require('slue-stream');
const babel = require('babel-core');

module.exports = function(opts = {}) {
    if (typeof opts.sourceMaps !== 'boolean') {
        opts.sourceMaps = false;
    }

    return slueStream.transformObj(function(file, env, cb) {
        let extname = path.extname(file.path);
        if (extname === '.js' || extname === '.jsx') {
            let contents = file.contents.toString();
            try {
                let result = babel.transform(contents, {
                    sourceMaps: opts.sourceMaps,
                    presets: [
                        require('babel-preset-env'),
                        require('babel-preset-react'),
                        require('babel-preset-stage-3')
                    ]
                });
                if (result.map) {
                    let relativePath = path.relative(process.cwd(), file.path);
                    let moduleId = relativePath.replace(/\\/g, '/');
                    result.map.sources = [`sluepack:///${moduleId}`];
                    file.sourceMap = result.map;
                    file.sourceMap.file = file.path;
                }

                file.contents = Buffer.from(result.code);
                cb(null, file);
            } catch (e) {
                cb(null, file);
            }
        } else {
            cb(null, file);
        }
    });
}