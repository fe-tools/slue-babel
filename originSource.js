const path = require('path');
const acorn = require('acorn');
const SourceMapGenerator = require('source-map').SourceMapGenerator;
const SourceMapConsumer = require('source-map').SourceMapConsumer;

module.exports = function(file, sourceMap) {
    let generator = new SourceMapGenerator();
    let contents = file.contents.toString();
    let tokenizer = acorn.tokenizer(contents, {
        locations: true
    });
    let source = path.relative(process.cwd(), file.path);
    source = source.replace(/\\/g, '/');
    source = `sluepack:///${source}`;
    while (true) {
        let token = tokenizer.getToken();
        if (token.type.label === "eof") {
            break;
        }
        let mapping = {
            original: token.loc.start,
            generated: token.loc.start,
            source: source
        };
        if (token.type.label === 'name') {
            mapping.name = token.value;
        }
        generator.addMapping(mapping);
    }
    generator.setSourceContent(source, contents);
    file.sourceMap = generator.toJSON();

    SourceMapConsumer.with(sourceMap, null, function(consumer) {
        let _generator = SourceMapGenerator.fromSourceMap(consumer);
        _generator.applySourceMap(new SourceMapConsumer(file.sourceMap));
        file.sourceMap = JSON.parse(_generator.toString());
        console.log(file.sourceMap);
    })
}