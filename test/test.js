const slueFs = require('slue-fs');
const slueStream = require('slue-stream');
const slueBabel = require('../index');

slueFs.read('./test/main.js').pipe(slueBabel()).pipe(slueStream.transformObj(function(file, env, cb) {
    console.log(file.contents.toString());
    cb(null, file);
}));