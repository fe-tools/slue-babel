# slue-babel
Transform es6 to es5.

## useage
```javascript
const slueFs = require('slue-fs');
const slueFs = require('slue-stream');
const slueBabel = require('slue-babel');

slueFs.read('./test/main.js').pipe(slueBabel());

slueFs.read('./test/main.js').pipe(slueBabel({
    sourceMaps: true  // default false
})).pipe(slueStream.transformObj(function(file, env, cb) {
    console.log(file.sourceMap);
    cb(null, file);
}));
```